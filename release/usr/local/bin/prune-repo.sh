#!/bin/bash

set -eu

OSTREE=/usr/bin/ostree

opts=()
for ref in $($OSTREE refs --list runtime/org.freedesktop.Sdk.PreBootstrap); do
  opts+=("--retain-branch-depth=${ref}=-1")
done

$OSTREE prune --refs-only --keep-younger-than="2 month ago" "${opts[@]}"
